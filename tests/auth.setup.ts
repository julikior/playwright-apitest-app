import { test as setup } from '@playwright/test';
import user from '../.auth/user.json'
import fs from 'fs'

const authFile = '.auth/user.json'

setup('authentication', async({request}) => {
    const response = await request.post('https://conduit-api.bondaracademy.com/api/users/login', {
        data: {
            user: {email: "juli@gmail.com", password: "123"}
        }
    })
    const responseBody = await response.json()
    const accessToken = responseBody.user.token

    // Update a user object with a new value
    user.origins[0].localStorage[0].value = accessToken
    fs.writeFileSync(authFile, JSON.stringify(user))

    //Assign accessToken value to process environment variable 
    process.env['ACCESS_TOKEN'] = accessToken

})

